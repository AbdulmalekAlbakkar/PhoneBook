# Phone Book (Basic + Advance)

## General

I used a material template to build this website, it provieds some UI tools to make things easier, especially the responsive issues.
It's only 2 pages (home and phonebook), added some features when I realised that there is an "advance" task for the same project.

### Search
Input at the top, Based on Jquery and JS directrly to filter the cards (contacts).

### Add/Edit
Modal window, same modal for add and edit then send an ajax request (post) to get the corresponding partial view for those information, it's easier than editing all info of the div contact
Used Preappend to add the new contact at the top of the list

### Sorting
Based on pure js, 2 buttons one asc and the other desc, simple as possible.

## Used Techniques

C#
Asp.net MVC
JSON
Post
Ajax
Jquery

## Used Templates
AdminBSB - Material Design.