﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectChallenge.Models
{
    public class Person
    {
        public string name { get; set; }
        public string phone_number { get; set; }
        public string address { get; set; }
    }
    public class People
    {
        [JsonProperty("contacts")]
        public List<Person> contacts { get; set; }
    }
}