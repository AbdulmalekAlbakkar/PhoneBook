﻿using Newtonsoft.Json;
using ProjectChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ProjectChallenge.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Phonebook()
        {
            return View(JsonConvert.DeserializeObject<People>(new WebClient().DownloadString("http://www.mocky.io/v2/581335f71000004204abaf83")).contacts.ToList());
        }
        public ActionResult GetContact(Person person)
        {
            return PartialView("_Contact", person);
        }
    }
}